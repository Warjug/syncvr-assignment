Thank you for taking the time to take a look at this project.

This is an application meant to demonstrate the Fibonacci sequence through
the use of Unity's Canvas elements.

Instructions:
Numbers will generate in the order of the Fibonacci sequence starting from 0.
Press the button that says "Press Me". A sound effect will play, and numbers
will start to generate onto the laptop's screen. 


Design Choices:
Within the virtual environnment, I opted to use a laptop asset to display Unity's Canvas. I felt that this would lead to a more engaging interaction with the application. This is also a meta-joke as I created this project on my own laptop. 

Regarding the audio, I felt it was fitting to have a form of nature ambience
to fit with the sunset skybox and overall warm tone of the scene. 

Download link to Build:
https://we.tl/t-lWNOWcbrm6
