﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FibonacciController : MonoBehaviour
{

    public Button _buttonOne;
    public Text _displayNumber;
    private AudioSource _audioSrc;

    int fn1 = 0;
    int fn2 = 0;

    // Start is called before the first frame update
    void Start()
    {
        _buttonOne.onClick.AddListener(HandleFibonacci);
        _displayNumber.text = "";
        _audioSrc = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void HandleFibonacci()
    {
        int c;

        //solves issue of 'fn1' and 'fn2' both being 0 at start
        if(fn1 == 0 && fn2 == 0)
        {
            fn2 = 1;
            c = 0;
        }
        else
        {
            //sum of previous 2 numbers
            c = fn1 + fn2;

            //find next Fibonacci number
            fn2 = fn1;
            fn1 = c;
        }
        _displayNumber.text = c.ToString();
        _audioSrc.PlayOneShot(_audioSrc.clip);

    }
}
